source "azure-arm" "Linux-16-04" {
  tenant_id = "190ce420-b157-44ae-bc2f-69563baa5a3b"
  subscription_id = "cbbb5627-ca60-48c8-af99-38dc687232df"
  
  managed_image_name = "Linux-16-04"
  managed_image_resource_group_name = "Linux-resources"

  os_type = "Linux"
  image_publisher = "Canonical"
  image_offer = "UbuntuServer"
  image_sku = "16.04-LTS"

  location = "East US"
  vm_size = "Standard_DS2_v2"

  azure_tags = {
    dept = "Engineering"
    task = "Image deployement"
  }
}
source "azure-arm" "Linux-18-04" {
  tenant_id = "190ce420-b157-44ae-bc2f-69563baa5a3b"
  subscription_id = "cbbb5627-ca60-48c8-af99-38dc687232df"

  managed_image_name = "Linux-18-04"
  managed_image_resource_group_name = "Linux-resources"

  os_type = "Linux"
  image_publisher = "Canonical"
  image_offer = "UbuntuServer"
  image_sku = "18.04-LTS"

  location = "East US"
  vm_size = "Standard_DS2_v2"

  azure_tags = {
    dept = "Engineering"
    task = "Image deployement"
  }
}

build {
  sources = ["sources.azure-arm.Linux-16-04"]
  provisioner "shell" {
    inline          = [
      "sudo apt-get update",
      "sudo apt-get upgrade -y",
      "sudo apt-get -y install apache2",

      "/usr/sbin/waagent -force -deprovision+user && export HISTSIZE=0 && sync"
    ]
  }
  provisioner "file" {
    source = "./wordpress.conf"
    destination = "/tmp/"
  }


  provisioner "shell" {
    script = "./deploy_wordpress.sh"
  }
}